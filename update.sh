KPS_CHART_VERSION="41.5.1"
KPS_NAMESPACE="monitoring"

# ------------------------------------------------

# Create namespace
kubectl create ns "${KPS_NAMESPACE}" --dry-run=client -o yaml | kubectl apply -f -

# Install Kube Prometheus Stack
helm repo add "prometheus" "https://prometheus-community.github.io/helm-charts"
helm repo update
helm upgrade --install --atomic --debug "prometheus" "prometheus/kube-prometheus-stack" \
  --version "${KPS_CHART_VERSION}" \
  --namespace "${KPS_NAMESPACE}" \
  --timeout 600s \
  --values "values.yaml"

# Check status
kubectl -n "${KPS_NAMESPACE}" rollout status deployment prometheus-kube-state-metrics
kubectl -n "${KPS_NAMESPACE}" rollout status deployment prometheus-kube-prometheus-operator
kubectl -n "${KPS_NAMESPACE}" rollout status deployment prometheus-grafana

# Manifests
kubectl apply -f manifests/basicauth.yaml
kubectl apply -f manifests/servicemonitors.yaml

# Dashboards
kubectl -n "${KPS_NAMESPACE}" create configmap "traefik-dashboard" --from-file="traefik-dashboard.json=./dashboards/traefik-dashboard.json" --dry-run=client -o yaml | kubectl apply -f -
kubectl -n "${KPS_NAMESPACE}" label --overwrite=true configmap "traefik-dashboard" "grafana_dashboard=true"