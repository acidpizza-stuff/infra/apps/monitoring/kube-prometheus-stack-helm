# Kube-Prometheus-Stack

## Update Kube-Prometheus-Stack

Reference: https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack#upgrading-chart

```bash
helm repo add "prometheus" "https://prometheus-community.github.io/helm-charts"
helm search repo prometheus/kube-prometheus-stack -l

./update.sh
```


## Access

Prometheus and Alert Manager are not exposed because they do not have authentication.

```bash
kubectl -n monitoring port-forward svc/prometheus-grafana 8080:80
kubectl -n monitoring port-forward svc/prometheus-kube-prometheus-prometheus 9090
kubectl -n monitoring port-forward svc/prometheus-kube-prometheus-alertmanager 9093
```

Access from browser at `localhost:8080`, `localhost:9090` and `localhost:9093` respectively.


## Traefik monitoring

https://githubmate.com/repo/cablespaghetti/k3s-monitoring/issues/9
https://github.com/grafana/grafana/issues/10786


## Dashboards

### Recreate Dashboard via ConfigMap

Find all dashboard configmaps.

```bash
kubectl -n monitoring get configmap -l grafana_dashboard=1
```

Create ConfigMap Manifest.

```yaml
# pod-count-dashboard.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  labels:
    grafana_dashboard: "1"
  name: grafana-k8s-pod-count
  namespace: prom
data:
  k8s-pod-count.json: |-
    {
      ...
      ...
      ...
    }
```

Create ConfigMap from json file.

```bash
# Create configmap
kubectl -n monitoring create configmap grafana-k8s-pod-count --from-file=k8s-pod-count.json
# Label configmap to be picked up by grafana sidecar
kubectl -n monitoring label configmap grafana-k8s-pod-count grafana_dashboard=1
```

### Importing Dashboards (Deprecated)

> This does not seem to be the case anymore

Dashboards downloaded from grafana labs will have datasource named `${DS_PROMETHEUS}`. This needs to be renamed to `Prometheus` else there would be error.


## Troubleshooting

https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/troubleshooting.md#troubleshooting-servicemonitor-changes

### Service Monitor Selector

`serviceMonitorSelector: {}` is supposed to target all service monitors. However, by default `serviceMonitorSelectorNilUsesHelmValues: true` causes the behaviour to be changed to target only service monitors with labels `release: prometheus`. Setting to `false` solves this behaviour.

To target all service monitors in all namespaces:

```yaml
prometheus:
  prometheusSpec:
    serviceMonitorSelectorNilUsesHelmValues: false
    serviceMonitorSelector: {}
    serviceMonitorNamespaceSelector: {}
```


## Traefik Basic Auth for Prometheus Dashboard

```bash
printf "prom:$(openssl passwd -apr1)\n" > pass.txt
# key in a password
kubectl --namespace monitoring create secret generic prometheus-auth --from-file=users=pass.txt
```

## Default Grafana Credentials

```text
admin:prom-operator
```